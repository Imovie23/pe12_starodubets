/**
 *
 * @desc txt about function
 * @param  {Number } a
 * @param {Number } b
 * @returns {Number}
 */

function sum(a, b) {
    return a + b;
}


/**
 *
 * @param {Number} a
 * @param {Number} b
 * @returns {Number}
 */

function multiply(a, b) {
    if (isFinite(a) && isFinite(b)){
        return a * b;
    }
    return 0;
}


/**
 * @desc Подсчитывает коло-во вхождений в строку
 * @param {String} str
 * @param {Number}num
 * @return {Number}
 */


function numInStr(str, num) {
    let counter = 0;

    for (let i = 0; i < str.length; i++) {
        let s = str.charAt(i);

        if (s !== ' ' && +s === num) {
            counter++;
        }
    }
    return counter;
}

/**
 *
 * @param {String}a
 * @param b
 * @return {number}
 */


function diff(a, b) {
    return a - b;

}


/**
 *
 * @param amountOfHits
 * @param goals
 * @return {number}
 */


function hockey(amountOfHits, goals) {



    if(!isFinite(amountOfHits) && !isFinite(goals)) {
        return 0;
    }
    return goals / amountOfHits * 100;
}