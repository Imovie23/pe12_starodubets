/**
 * @desc Функция для запуска скрипта после загрузки всего DOM jQuery
 * @param {String} value
 */

$(function () {

    const tabsTitle = $('.tabs-title');
    let arrTest = [];
    let count = 0;


    /**
     * @desc Переключение вкладок для section service event click
     */

    tabsTitle.on('click', function (event) {

        const clickTab = event.currentTarget;
        const removeActive = $('.tabs-title.active');
        $(removeActive).removeClass('active');
        if (clickTab) {
            $(clickTab).addClass('active');
            const activeTab = $('.active');
            const textActivated = $('.tabs-txt');

            const activeTabData = $(activeTab).data('name');


            $(textActivated).each(function () {
                const textActivatedElement = $(this).data('name');

                if (activeTabData === textActivatedElement) {
                    $(this).removeAttr('hidden');
                    $(this).css('display', 'flex');
                } else {
                    $(this).attr('hidden', 'hidden');
                    $(this).css('display', 'none');
                }

            })

        }

    });


    /**
     * @desc Функция для рандомного числа от 1 до 25
     * @return {number}
     */

    function mathRand() {
        return Math.floor(Math.random() * 24) + 1;
    }

    /**
     * @desc Функция для рандомного атрибута для присвоения картинке section our-work
     * @return {String}
     */
    function randDataName() {
        const arrAttr = ['graphicDesign', 'webDesign', 'landingPages', 'wordpress'];
        let randAttr = Math.floor(Math.random() * arrAttr.length);
        return arrAttr[randAttr];
    }

    const buttonLoad = $('.btn_software.softwareClick');

    /**
     * @desc Функция для загрузки картинок 12 картинок по event click на кнопку
     *
     */

    buttonLoad.on('click', function (e) {
        const galleryOurMost = $('.gallery-our-most');
        let galleryLength = $('.gallery-our-most img').length;
        const animationLoad = $('.animation-load');

        animationLoad.css('display', 'block');
        buttonLoad.css('display', 'none');

        /**
         * @desc Таймер для имитации загрузки картинок с сервера section our-work
         * @type {number}
         */

        let timerLoad = setTimeout(function () {
            animationLoad.css('display', 'none');
            for (let i = 0; i < 12; i++) {
                const img = document.createElement('img');
                img.src = `img/AmazingWorkLoad/${mathRand()}.jpg`;
                img.setAttribute('data-name', randDataName());

                const wrapperAbout = document.createElement('div');
                wrapperAbout.classList.add('wrapper-about');

                const aboutSectionWork = document.createElement('div');
                aboutSectionWork.classList.add('about_section_work');

                const linkImgAbout = document.createElement('span');
                linkImgAbout.classList.add('link-img-about');

                const chainLogoGallery = document.createElement('a');
                chainLogoGallery.classList.add('chain_logo_gallery');
                chainLogoGallery.setAttribute('href', '#');

                const aLink = document.createElement('a');
                aLink.setAttribute('href', '#');

                const iFaLink = document.createElement('i');
                iFaLink.classList.add('fas', 'fa-link');

                const iFaSearch = document.createElement('i');
                iFaSearch.classList.add('fas', 'fa-search');

                const txtFirstAbout = document.createElement('span');
                txtFirstAbout.classList.add('txt_first_about');
                txtFirstAbout.innerText = 'creative design';

                const txtSecondAbout = document.createElement('span');
                txtSecondAbout.classList.add('txt_second_about');

                wrapperAbout.appendChild(aboutSectionWork);
                aboutSectionWork.appendChild(linkImgAbout);
                linkImgAbout.appendChild(chainLogoGallery);
                linkImgAbout.appendChild(aLink);
                chainLogoGallery.appendChild(iFaLink);
                aLink.appendChild(iFaSearch);
                aboutSectionWork.appendChild(txtFirstAbout);
                aboutSectionWork.appendChild(txtSecondAbout);

                arrTest.push(img);
                const activeFilter = $('.activeFilter').data('name');

                if (activeFilter === 'all') {
                    wrapperAbout.appendChild(img);
                    galleryOurMost.append($(wrapperAbout));
                } else {
                    const imgs = getList();
                    const galleryOurMost = document.getElementsByClassName('gallery-our-most')[0];
                    for (let i in imgs) {
                        let imgClass = imgs[i];
                        imgClass.classList.add('imgClass');
                        galleryOurMost.appendChild(imgClass);
                    }

                }

                if (galleryLength < 36) {
                    txtSecondAbout.innerText = img.getAttribute('data-name');
                    if (galleryLength < 24) {
                        buttonLoad.css('display', 'flex');
                    }
                } else {
                    buttonLoad.css('display', 'none');
                    clearTimeout(timerLoad);
                    break;
                }

            }

            /**
             * @desc Функция для фильтрации картинок по типу section our-work
             * @return {*[]}
             */

            function getList() {
                return arrTest.filter(function (i) {
                    const activeFilter = $('.activeFilter').data('name');
                    const arrData = $(i).data('name');
                    if (arrData === activeFilter) {
                        return i;
                    } else if (activeFilter === 'all') {
                        return i;
                    }
                })
            }
        }, 2000);
        e.preventDefault();

    });


    const filterImg = $('.filterImg');

    /**
     * @desc Функция для фильтрации картинок по нажатию на соответствующий Tabs section our-work
     */

    $(filterImg).on('click', function (event) {

        const clickTab = event.currentTarget;
        const removeActive = $('.filterImg.activeFilter');
        $(removeActive).removeClass('activeFilter');

        if (clickTab) {
            $(clickTab).addClass('activeFilter');
            const activeTab = $('.activeFilter');
            const textActivated = $('.wrapper-about');
            const activeTabName = $(activeTab).data('name');
            const imgClass = $('.imgClass');


            imgClass.each(function () {
                let imgList = $(this).data('name');
                if (imgList === activeTabName) {
                    $(this).removeAttr('hidden');
                } else if (activeTabName === 'all') {
                    $(this).removeAttr('hidden');
                } else if (imgList !== activeTabName) {
                    $(this).attr('hidden', 'hidden');
                }
            });

            textActivated.filter(function () {
                const elemDataName = $(this).find('img').data('name');

                if (elemDataName === activeTabName) {
                    $(this).removeAttr('hidden');

                } else if (activeTabName === 'all') {
                    $(this).removeAttr('hidden');


                } else {
                    $(this).attr('hidden', 'hidden');

                }

            })

        }

        event.preventDefault();
    });


    const arrCarouselReviews = [];

    /**
     * @desc Функция для создания динамичести карусели на странице section reviews-about-us
     * @return {Array}
     */

    function arrCarousel() {

        let elemCarousel = $('.border_img_preview');

        for (let i = 0; i < elemCarousel.length; i++) {
            $(elemCarousel).removeClass('border_img_preview');
            $(elemCarousel).addClass('border_img_preview_js');
            arrCarouselReviews.push(elemCarousel[i]);
        }
        return arrCarouselReviews;
    }

    /**
     * @desc Функция для добавления элементов на страницу section reviews-about-us
     * @param arr
     */

    function buildCarousel(arr) {
        const ElemCarouselChildren = $('.carousel_children_none');

        let imgList = arr;
        const carouselWrapper = $('.carousel_wrapper');

        for (let i = 0; i <= imgList.length; i++) {
            $(ElemCarouselChildren).append(imgList[i]);
        }

        $(carouselWrapper).append(ElemCarouselChildren);
    }

    /**
     * @desc Функция для удаления и добавления элему класса activeCarousel reviews-about-us
     */

    function animationCarousel() {

        const btnLeft = $('.btn_left');
        const btnRight = $('.btn_right');
        const carouselChildren = $('.carousel_children');
        let correctArrPosition = arrCarousel();
        const addClassActive = $('.border_img_preview_js');
        const borderImgReviews = $('.border_img_reviews img');
        const currentLeftValue = 0;
        const ElemCarouselChildrenClick = $('.clickTarget');


        function classActive(elem) {
            addClassActive.each(function () {
                if (elem === this) {

                    $(this).addClass('activeCarousel');
                    $(this).find('.border_second_preview').addClass('activeCarousel');
                    const imgSrcCarousel = $(this).find('img');

                    if (imgSrcCarousel.attr('src') !== borderImgReviews.attr('src')) {
                        borderImgReviews.attr('src', imgSrcCarousel.attr('src'));
                    }

                } else {
                    $(this).removeClass('activeCarousel');
                    $(this).find('.border_second_preview').removeClass('activeCarousel');

                }


            })
        }

        /**
         * @desc Функция для переключения текста в section reviews-about-us
         */

        function textActive() {

            const ActiveCarou = $('.blockquote-txt');
            const activeCar = $('.activeCarousel').data('name');

            $(ActiveCarou).each(function () {
                const textElement = $(this).data('name');


                if (textElement === activeCar) {
                    $(this).removeAttr('hidden');

                } else {
                    $(this).attr('hidden', 'hidden');

                }

            })

        }


        /**
         * @desc event click для левой кнопки для переключения картинок в карусели
         */
        btnLeft.on('click', function () {
            textActive();
            carouselChildren.remove();
            if (currentLeftValue === 0) {
                const elemFirst = correctArrPosition.pop();
                correctArrPosition.unshift(elemFirst);
                buildCarousel(correctArrPosition);
            }

            correctArrPosition.forEach(function (i) {
                if ($(i).hasClass('activeCarousel')) {
                    let elementArr = $(i)[0];
                    let elementIndex = correctArrPosition.indexOf($(elementArr).prev()[0]);

                    classActive(correctArrPosition[elementIndex]);
                }

            });

        });

        /**
         *@desc event click для правой кнопки для переключения картинок в карусели
         */

        btnRight.on('click', function () {
            textActive();
            carouselChildren.remove();
            if (currentLeftValue === 0) {
                const elemFirst = correctArrPosition.shift();
                correctArrPosition.push(elemFirst);
                buildCarousel(correctArrPosition);
            }

            const test = $('.activeCarousel');

            correctArrPosition.forEach(function () {

                if ($(test).hasClass('activeCarousel')) {

                    let elementArr = $(test)[0];
                    let elementIndex = correctArrPosition.indexOf($(elementArr).next()[0]);

                    console.log(elementIndex);

                    if (elementIndex === -1) {
                        classActive(correctArrPosition[0]);
                    } else {
                        classActive(correctArrPosition[elementIndex]);
                    }

                }
            });
        });

        ElemCarouselChildrenClick.on('click', function (event) {

            const currentElem = event.target;
            const parentElem = $(currentElem).parent().parent()[0];

            if (currentElem === this) {
                return false;
            }

            classActive(parentElem);
            textActive();


        })


    }


    animationCarousel();

    /**
     * @desc plugin masonry
     * @type {jQuery}
     */

    let $grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 370,
        gutter: 17,
    });


    $grid.on('click', '.grid-item', function (e) {
        let elemTarget = e.target;

        if ($(elemTarget).parent().hasClass('grid-item-child')) {
            e.preventDefault()
        } else if ($(elemTarget).hasClass('correct_margin') || $(elemTarget).hasClass('img_dimensions')) {
            e.preventDefault()
        } else {
            $(this).toggleClass('gigante');
        }

        $grid.masonry('layout');
    });


    let btnClick = $('.append-button');

    /**
     * @desc Функция для добавления новых картинок в конец section gallery-best-img
     */

    btnClick.on('click', function (e) {
        count++;

        const animationLoadGallery = $('.animation-load-gallery');

        animationLoadGallery.css('display', 'block');
        btnClick.css('display', 'none');

        let timerLoadTwo = setTimeout(function () {

            animationLoadGallery.css('display', 'none');

            if (count === 2) {
                btnClick.css('display', 'none');
                clearTimeout(timerLoadTwo);
            } else {
                btnClick.css('display', 'flex');
            }

            for (let i = 0; i < 12; i++) {
                let elems = [getItemElement()];
                let $elems = $(elems);
                $grid.append($elems).masonry('appended', $elems);
            }

        }, 2000);
        console.log(count === 2);


        e.preventDefault();
    });

    /**
     * @desc Функция для построения картинок на страницу section gallery-best-img
     * @return {HTMLDivElement}
     */

    function getItemElement() {
        let elem = document.createElement('div');
        let imgMans = document.createElement('img');
        imgMans.src = `img/AmazingWorkLoad/${mathRand()}.jpg`;

        let widthClass = `grid-item--width2`;
        let heightClass = `grid-item--height2`;
        elem.className = 'grid-item ' + widthClass + ' ' + heightClass;

        $(elem).append(imgMans);
        return elem;
    }


});
