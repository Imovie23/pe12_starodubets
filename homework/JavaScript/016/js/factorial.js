

let numFactorial = +prompt('Enter a factorial number', '1 - 999');
let result = 0;
let concatFactorial = '';


for (let i = 0; i < 5; i++) {
    if (isNaN(+numFactorial)) {
        numFactorial = +prompt('Enter a correct factorial number', '1 - 999');
    }
}

/**
 * @desc Факториал числа
 * @param {Number}number
 * @return {number}
 */

function factorial(number) {
    if (+number !== 0 && +number !== 1) {
        return number * factorial(number - 1);
    }else if (+number === 1 || +number === 0){
        return number;
    }
}

result = factorial(numFactorial);

for (let i = 2; i <= numFactorial; i++){
 if ( i < numFactorial) {
     concatFactorial += i + ' *' + ' ';
 }else {
     concatFactorial += i;
 }

}


console.log(`${numFactorial}! = ${concatFactorial} = ${result}`);



