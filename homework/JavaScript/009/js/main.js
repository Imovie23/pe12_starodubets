document.addEventListener('DOMContentLoaded', onReady);


/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 * @param {String} value
 */

function onReady(value) {

    const tabsTitle = document.getElementsByClassName('tabs');
    const tabsTitleElement = tabsTitle[0];
    console.log(tabsTitleElement);

    tabsTitleElement.addEventListener('click', onTabsClick);

    /**
     * @desc  Событие onclick, для переключение tabs и текста для конкретной вкладки
     * @param event
     */
    function onTabsClick(event) {
        const clickTab = event.target.classList;

        const removeActive = document.getElementsByClassName('active')[0];
        removeActive.classList.remove('active');

        if (clickTab) {
            clickTab.add('active');
            const activeTab = document.getElementsByClassName('active')[0];
            const textActivated = document.getElementsByClassName('tabs-txt');
            const activeTabData = activeTab.dataset.name;

            for (let i of textActivated) {
                const textActivatedElement = i.dataset.name;

                if (activeTabData === textActivatedElement) {
                    i.removeAttribute('hidden');
                } else {
                    i.setAttribute('hidden', value);
                }

            }


        }
    }

}

