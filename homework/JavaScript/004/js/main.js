let firstName = prompt('Enter your name', 'Name');
let lastName = prompt('Enter your surname ', 'Surname');


/**
 * @desc  Создание функции для записи обьекта пользователя
 * @param {String} name
 * @param {String} surname
 * @return {Object}
 */

function createNewUser(name, surname) {
    let newUser = {
        login: function getLogin() {
            return this.firstName.charAt(0).toLocaleLowerCase() + this.lastName.toLowerCase();
        },

        setFirstName: function (obj, value) {
            let result = Object.assign({}, obj);

            return Object.defineProperty(result, 'firstName', {
                enumerable: true,
                configurable: true,
                value
            });
        },
        setLastName: function (obj, value) {
            let result = Object.assign({}, obj);

            return Object.defineProperty(result, 'lastName', {
                enumerable: true,
                configurable: true,
                value
            });

        }
    };


    Object.defineProperty(newUser, 'firstName', {
        enumerable: true,
        configurable: true,
        value: name
    });

    Object.defineProperty(newUser, 'lastName', {
        enumerable: true,
        configurable: true,
        value: surname
    });

    console.log('firstName  -->',newUser.firstName = 'kk');
    console.log('setFirstName --->', newUser.setFirstName(newUser,'ddd'));

    console.log(newUser.login());
    // console.log(newUser);
}

console.log(newUser);


createNewUser(firstName, lastName);

