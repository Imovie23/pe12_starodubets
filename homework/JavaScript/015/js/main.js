/**
 * @desc Функция для запуска скрипта после загрузки всего DOM jQuery
 * @param {String} value
 */
$(function () {

    const $menu = $('.top_menu_hw'),
        $page = $('html, body'),
        buttonTop = $('<button>').attr({'type': 'button', 'class': 'button-top'});

    $(buttonTop).text('To top')
        .hide()
        .on('click', function () {
            $page.animate({
                scrollTop: 0
            }, 3000);
        });

    $('header').append($(buttonTop));

    /**
     * @desc Функция показа кнопки после прокрутки больше чем на один экран
     */

    $(window).scroll(function () {
        if ($(this).scrollTop() > $(window).height()) {
            $(buttonTop).show();
        } else {
            $(buttonTop).hide();
        }
    });



    /**
     * @desc Функция прокрутки страницы по клику на ссылку до указанного места
     */

    $($menu).on('click', function (e) {
        let thisElem = $(this);

        $page.animate({
            scrollTop: $(thisElem.attr('href')).offset().top

        }, 3000);

        e.preventDefault();
    });


    const btnToggle = $('.button-toggle');

    /**
     * @desc Функция включения и выключения раздела
     */

    btnToggle.on('click', function () {
        $('.clients').toggle();
    })


});