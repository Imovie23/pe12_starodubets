/**
 * 1)setTimeout() - выполняет один раз с задержкой указанной в параметре,
 * setInterval() - вызывает функцию каждый раз с задержкой указанной в параметре.
 *
 * 2)если в функцию setTimeout() передать нулевую задержку - она не сработает потому что минимальная задержка 4 милиссек,
 * это сделала из за того что много сайтов на момент когда хотели изменить задержку были повязаны на 0 задержке.
 * Она будет выполнять сразу как только прогрузиться элемент страницы.
 *
 *3)clearInterval() - если функцию не вызывать то setTimeout и setInterval буду работать безконечно и татратить рессурсы компьютера,
 * если один таймер то будет не так заметна задержка, но если будет их много то приложение может зависнуть.
 *
 */



document.addEventListener('DOMContentLoaded', onReady);


/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 * @param {String} value
 */
function onReady() {

    const imgWrap = document.getElementsByClassName('images-wrapper')[0];
    const parentImg = imgWrap.children;
    const body = document.body;
    let timerId;
    let inInterval;

    timerFunc();

    /**
     * @desc Функция для затухания картинки (анимация)
     * @param elem
     * @param speed
     */
    function fadeOut(elem, speed) {
        if (!elem.style.opacity) {
            elem.style.opacity = '1';
        }
        setTimeout(function () {
            let outInterval = setInterval(function () {
                elem.style.opacity -= '0.02';
                if (elem.style.opacity <= 0) {
                    clearInterval(outInterval);
                }
            }, speed / 50);
        }, 9000);


    }

    /**
     * @desc Функция для появления картинки с opacity 0 до opacity 1 (анимация)
     * @param elem
     * @param speed
     */

    function fadeIn(elem, speed) {
        if (!elem.style.opacity) {
            elem.style.opacity = '0';
        }
        inInterval = setInterval(function () {
            elem.style.opacity = Number(elem.style.opacity) + 0.02;
            if (elem.style.opacity >= 1)
                clearInterval(inInterval);
        }, speed / 50);
    }

    fadeIn(parentImg[0], 1000);
    fadeOut(parentImg[0], 1000);

    /**
     * @desc Функция рекурсивного таймера вывода картинок
     */
    function timerFunc() {
        timerId = setTimeout(function slider() {
            for (let element of parentImg) {
                if (element.classList.contains('active')) {
                    element.classList.remove('active');
                    if (element.parentNode.lastElementChild === element) {
                        element.parentNode.firstElementChild.classList.add('active');
                        fadeIn(parentImg[0], 1000);
                        fadeOut(parentImg[0], 1000);
                    } else {
                        const next = element.nextElementSibling;
                        if (next) {
                            next.classList.add('active');
                            fadeIn(next, 1000);
                            fadeOut(next, 1000);
                        }

                    }

                    break;
                }

            }
            timerId = setTimeout(slider, 10000);
        }, 10000);
    }

    /**
     * @desc Функция таймер для появления кнопок на страницу через 0.5 сек
     */

    setTimeout(function btn() {
        let btnClick = document.createElement('button');
        let cloneBtn = btnClick.cloneNode(false);

        btnClick.type = 'button';
        btnClick.innerText = 'Stop timer';
        body.appendChild(btnClick);

        cloneBtn.type = 'button';
        cloneBtn.innerText = 'Start';
        cloneBtn.style.cssFloat = 'right';
        body.appendChild(cloneBtn);

        btnClick.addEventListener('click', onStopClick);
        cloneBtn.addEventListener('click', onStartClick);


        /**
         * @desc Функция onClick для остановки показа слайдера и таймера
         * @param event
         */

        function onStopClick(event) {
            if (event) {
                clearTimeout(timerId);

                pause = true;

            }
        }

    }, 500);

    /**
     * @desc Функция onClick для возобновления показа слайдера и таймера
     * @param event
     */

    function onStartClick(event) {
        if (event) {
            setTimeout(timerFunc, 1000);
            pause = false;
            requestAnimationFrame(startTime);

        }
    }

    let timeElem = document.getElementById('time'),
        countdown = new Date(),
        responseTime = (new Date(Date.now() + (1000 * 10))),
        pause = false;

    /**
     * @desc Функция для показа таймера секунд и миллисекунд
     *
     */





    function startTime() {
        countdown.setTime(responseTime - Date.now());
        timeElem.innerHTML = countdown.getSeconds() + ':' + countdown.getMilliseconds();
        if (pause) return;

        if (countdown.getSeconds() >= 0 && countdown.getMinutes() < 1) {
            requestAnimationFrame(startTime);
        } else {
            // responseTime = new Date(Date.now() + (1000 * 10));
            countdown.setTime(responseTime - Date.now());
            requestAnimationFrame(startTime);

        }
    }

    requestAnimationFrame(startTime);
}