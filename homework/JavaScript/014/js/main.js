/**
 * @desc Функция для запуска скрипта после загрузки всего DOM jQuery
 * @param {String} value
 */

$(function () {

    const tabsTitle = $('.tabs');
    console.log(tabsTitle);

    /**
     * @desc  Событие onclick jQuery, для переключение tabs и текста для конкретной вкладки
     * @param event
     */

    $(tabsTitle).on('click', function (event) {

        const clickTab = event.target;
        const removeActive = $('.tabs-title.active');
        $(removeActive).removeClass('active');

        if (clickTab) {
            $(clickTab).addClass('active');
            const activeTab = $('.active');
            const textActivated = $('.tabs-txt');

            const activeTabData = $(activeTab).data('name');

            $(textActivated).each(function () {
               const textActivatedElement = $(this).data('name');

                if(activeTabData === textActivatedElement){

                    $(this).removeAttr('hidden');
                }else {
                    $(this).attr('hidden', 'hidden');
                }

            })

        }

    })


});
