/**
 *
 *  экранирование это то как мы можем записать в string зарезервированные символы что
 *  бы они корректно отображались при выводе "\/" или '\''
 *
 */




let firstName = prompt('Enter your name', 'Name');
let lastName = prompt('Enter your surname ', 'Surname');
let dateOfBirth = prompt('Enter your Birthday', 'dd.mm.yyyy');
let result = 0;


/**
 * @desc  Создание функции для записи обьекта пользователя, подсчета возраста и создание пароля
 * @param {String} name
 * @param {String} surname
 * @param {String} birth
 * @return {Object}
 */

function createNewUser(name, surname, birth) {
    let newUser = {
        login: function getLogin() {
            return this.firstName.charAt(0).toLocaleLowerCase() + this.lastName.toLowerCase();
        },

        setFirstName: function (obj, value) {
            let result = Object.assign({}, obj);

            return Object.defineProperty(result, 'firstName', {
                enumerable: true,
                configurable: true,
                value
            });
        },
        setLastName: function (obj, value) {
            let result = Object.assign({}, obj);

            return Object.defineProperty(result, 'lastName', {
                enumerable: true,
                configurable: true,
                value
            });
        },
        getAge: () => {

            while (dateOfBirth.charAt(2) !== '.' || dateOfBirth.charAt(5) !== '.' || dateOfBirth.length > 10 || dateOfBirth.length < 10) {
                dateOfBirth = prompt('Enter a correct your Birthday', 'dd.mm.yyyy')
            }
            let dateParts = birth.split(".");
            let dateOfBirthUser = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
            let nowDay = new Date(Date.now());

            if (dateOfBirthUser.getMonth() >= nowDay.getMonth()) {
                result = nowDay.getFullYear() - dateOfBirthUser.getFullYear() - 1;
            } else {
                result = nowDay.getFullYear() - dateOfBirthUser.getFullYear();
            }


            return `Users ${result} years`;

        },

        getPassword: () => {
            return name.charAt(0).toUpperCase() + surname.toLowerCase() + birth.slice(6).toString();
        }
    };



Object.defineProperty(newUser, 'firstName', {
    enumerable: true,
    configurable: true,
    value: name
});

Object.defineProperty(newUser, 'lastName', {
    enumerable: true,
    configurable: true,
    value: surname
});


console.log('firstName  -->', newUser.firstName = 'kk');
console.log('setFirstName --->', newUser.setFirstName(newUser, 'ddd'));

console.log(newUser.login());
console.log(newUser.getAge());
console.log(newUser.getPassword());
}


createNewUser(firstName, lastName, dateOfBirth);

