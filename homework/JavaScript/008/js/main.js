document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    let inputs = document.getElementById('price');
    let body = document.body;
    let span,
        spanPrice;


    inputs.addEventListener('focus', onPriceFocus);

    function onPriceFocus() {
        let div = document.getElementById('parentDiv');
        div.style.backgroundColor = 'green';
        inputs.style.cssText = 'margin: 12px;';
        if (span) {
            body.removeChild(span);
        }

        inputs.addEventListener('blur', onPriceBlur);

        function onPriceBlur() {
            if (inputs.value <= 0 || !+inputs.value) {
                div.style.backgroundColor = 'red';
                span = document.getElementById('spanError');
                if (!span) {
                    span = document.createElement('span');
                    span.id = 'spanError';
                    span.innerText = 'Please enter correct price';
                    span.style.color = 'red';
                    span.style.margin = '22px';
                    body.appendChild(span);
                }

            } else if (+inputs.value) {

                spanPrice = document.getElementById('spanPrice');
                if (!spanPrice) {
                    spanPrice = document.createElement('span');
                    spanPrice.id = 'spanPrice';
                    spanPrice.style.cssText = 'height: 30px; ' +
                        'border: 1px black solid; border-radius: 5px; padding: 3px; margin-left: 10px;';
                    spanPrice.innerHTML = `Текущая цена: ${inputs.value}$`;
                    body.insertBefore(spanPrice, div);

                    let spanBtn = document.getElementById('spanBtn');
                    if(!spanBtn) {
                        spanBtn = document.createElement('span');
                        spanBtn.id = 'spanBtn';
                        spanBtn.innerText = 'x';
                        spanBtn.style.cssText = 'width: 19px; height:19px; border: 1px black solid; border-radius: 50%; ' +
                            'margin-left: 10px;cursor:pointer; display: inline-block; text-align: center;';
                        spanPrice.appendChild(spanBtn);

                        spanBtn.addEventListener('click', onCloseClick);

                        function onCloseClick() {
                            body.removeChild(spanPrice);
                            inputs.value = '';
                            div.style.backgroundColor = 'white'
                        }

                    }
                }

            }


        }


    }
}
