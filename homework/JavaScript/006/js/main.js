/**
 * forEach - работает по такому же принципу как и цикл for(i = 0; i<10; i++) переберает все елементы масива
 *
 */

let arr = [12, '1', 'string', 34, true, false, null, 's', 'd', null, undefined, NaN, [1, 2, 4, 5]];


console.log(arr);

function filterBy(array, dataType) {
    let positiveArr;

    positiveArr = array.filter(function (arr) {
        if ((dataType === 'null') && (typeof arr === 'object')) {
            return array - 1;
        } else if ((dataType === 'Array') && (typeof arr === 'object') && Array.isArray(arr)) {
            return array;

        } else if (typeof arr !== dataType) {
            return array;
        }
    });
    console.log(positiveArr);

}

filterBy(arr, 'number');
