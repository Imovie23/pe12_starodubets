/**
 * DOM работа с динамически изменяющимися элементами на странице
 * работа с узлами HTML страницы добавление, изменение , удаление
 *
 */



let arr = ['1', '2', '3', 'sea', 'user', 23, {k: {i: 927, p: 112}},
    {
        x: {
            q: 9227,
            z: 111231232,
            x: 1121321332,
            po: 1145745732,
            zgg: 1137807802,
        }
    }, [221, 2114, 4576, 89]];

console.log(arr);


let ul = document.createElement('ul');
let parentElement = document.body;

/**
 * @desc Функция получает массив и выводит на страницу в виде списка
 * @param {Array} array
 * @return {Uint8Array | BigInt64Array | any[] | Float64Array | Int8Array | Float32Array | Int32Array | Uint32Array | Uint8ClampedArray | BigUint64Array | Int16Array | Uint16Array}
 */


function arrayList(array) {
    let newList;

    newList = array.map(function (i) {
        let li = document.createElement('li');


        if (typeof i === 'object') {
            if (Array.isArray(i)) {
                li.innerHTML = 'Array';
                ul.appendChild(li);
                for (let key in i) {
                    console.log('key -->',i[key]);
                    let li2 = document.createElement('li');
                    let ul2 = document.createElement('ul');

                    li2.innerHTML = i[key];
                    li.appendChild(ul2);
                    ul2.appendChild(li2);
                }
            }else {

                for (let key in i) {
                    li.innerHTML = key;
                    ul.appendChild(li);
                    result = i[key];

                    if (typeof result === 'object') {
                        for (let key in result) {
                            let li2 = document.createElement('li');
                            let ul2 = document.createElement('ul');
                            li2.innerHTML = key;
                            li.appendChild(ul2);
                            ul2.appendChild(li2);

                            let li3 = document.createElement('li');
                            let ul3 = document.createElement('ul');
                            li3.innerHTML = result[key];
                            li2.appendChild(ul3);
                            ul3.appendChild(li3);

                        }


                    }

                }
            }

        } else {
            li.innerHTML = `<b>${i}</b>`;
            return ul.appendChild(li);

        }


    });
    return newList;
}


console.log(arrayList(arr));

parentElement.appendChild(ul);


let timer = document.createElement('p');

timer.innerText = '10';

parentElement.appendChild(timer);


/**
 * @desc Таймер очистки страницы через 10 сек
 * @type {number}
 * @return {number}
 */

let timerOut = setInterval(function () {

    if (timer.innerText === '0') {
        parentElement.removeChild(ul);
        clearInterval(timerOut);
        timer.innerText = '0';


    } else {
        timer.innerText -= '1';
    }

}, 1000);






