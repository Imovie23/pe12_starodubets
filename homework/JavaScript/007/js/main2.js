/**
 * DOM работа с динамически изменяющимися элементами на странице
 * работа с узлами HTML страницы добавление, изменение , удаление
 *
 */



let arr = ['1','wood', '434', '3', 'sea', ['test', 'cool'], true, ['arr', 'loop', ['hello', 'world'], 'new', 'list'], 'oi', 23];

console.log(arr);



let listInDOM = arr.map(function (key) {
    if (typeof key === 'object') {
        return key;
    } else {
        return key;
    }
});

/**
 * @desc Функция получает массив и выводит на страницу в виде списка
 * @param {Array} array
 * @return {Array}
 */

function newList(arr) {
    let ul = document.createElement('ul');

    for (let i in arr) {
        if (Array.isArray(arr[i])) {
            let li2 =  newList(arr[i])
            ul.appendChild(li2);

        } else {
            let li = document.createElement('li');
            li.innerHTML = arr[i];
            ul.appendChild(li);
        }
    }

    return ul;
}


/**
 * @desc добавление на страницу ul
 * @param {Array}arr
 */
let parent = document.body;

function addListDOM(arr){
    parent = document.body;
    parent.appendChild(newList(arr));
}

addListDOM(arr);
newList(listInDOM);


let timer = document.createElement('p');
parent.appendChild(timer);

timer.innerText = '10';



/**
 * @desc Таймер очистки страницы через 10 сек
 * @type {number}
 * @return {number}
 */

let timerOut = setInterval(function () {

    if (timer.innerText === '0') {
        let ul = document.getElementsByTagName('ul');
        parent.removeChild(ul[0]);
        clearInterval(timerOut);
        timer.innerText = '0';


    } else {
        timer.innerText -= '1';
    }

}, 1000);






