document.addEventListener('DOMContentLoaded', onReady);


/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 * @param {String} value
 */

function onReady() {

    const iImg = document.getElementsByTagName('i');
    const inputValue = document.getElementsByTagName('input');
    const btnClick = document.getElementsByTagName('button')[0];

    for (let iShow of iImg) {
        iShow.addEventListener('mousedown', onImgMouseDown);
        iShow.addEventListener('mouseover', onImgMouseOver);
        iShow.addEventListener('mouseup', onImgMouseUp);

        /**
         * @desc event mouseover for cursor: pointer
         */
        function onImgMouseOver() {
            iShow.style.cursor = 'pointer';
        }

        /**
         * @desc Функция для изменения иконок и показа пароля в поле для ввода при нажатии на иконку
         */
        function onImgMouseDown() {
            if (iShow) {
                iShow.classList = 'fas fa-eye-slash icon-password';
                if (iShow.dataset.name === 'first') {
                    inputValue[0].type = 'text';
                } else {
                    inputValue[1].type = 'text';
                }

            }
        }
        /**
         * @desc Функция для возвращения на место иконки и поля для пароля
         */

        function onImgMouseUp() {
            if (iShow) {
                iShow.classList = 'fas fa-eye icon-password';
                if (iShow.dataset.name === 'first') {
                    inputValue[0].type = 'password';
                } else {
                    inputValue[1].type = 'password';
                }

            }
        }


    }



    btnClick.addEventListener('click', onBtnClick);
    const div = document.createElement('div');
    const body = document.body;
    /**
     * @desc Функция для проверки совпадения пароля
     */
    function onBtnClick() {

        if (inputValue[0].value === inputValue[1].value) {
            if(inputValue[0].value !== ''){
            alert('You are welcome');
            }
            body.removeChild()
        } else {
            div.innerText = 'Нужно ввести одинаковые значения';
            div.style.color = 'red';
            body.appendChild(div);


        }

    }


}