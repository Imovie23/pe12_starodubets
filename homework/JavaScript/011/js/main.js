/**
 * Из за того что он не корректно работает. Например пользователь может не вводить текст, а скопировать и вставить через мышку.
 *
 **/



    const charBtn = document.getElementsByClassName('btn');
    const body = document.body;

    body.addEventListener('keypress', onBtnKeyPress);

    /**
     * @desc Функция переключения букв с клавиатуры через событие keypress
     * @param event
     */
    function onBtnKeyPress(event) {
        const btnEvent = event.key.toUpperCase();
        for (let i of charBtn) {
            const charNum = i.innerText.toUpperCase();

            if (btnEvent === charNum) {
                i.classList.add('active');
            } else {
                i.classList.remove('active');
            }
        }

}