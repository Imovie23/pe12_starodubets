let obj = {
    h: 1,
    j: 90,
    l: 120,
    k: {
        i: 97,
        p: 12
    },
    oi : [[1,34,7], [55,43,22]]
};


console.log('obj --->', obj);


/**
 * @desc рекурсивная функция полного клонирования объекта
 * @param {Object} object
 * @param {Object} result
 */

function cloneObj(object) {
    let cloneObject = {};

    for (let key in object) {
        if (typeof (object[key]) == 'object') {
            if (Array.isArray(object[key])) {
                cloneObject[key] = cloneObj(object[key]) ;
            }else{
                cloneObject[key] = cloneObj(object[key]);}
        }else {
            cloneObject[key] = object[key];
        }

    }

    return cloneObject;

}

console.log('cloneObj function --->', cloneObj(obj));


