document.addEventListener('DOMContentLoaded', onReady);


/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 * @param {String} value
 */
function onReady() {


    const btnChange = document.getElementsByClassName('buttonPress')[0],
        body = document.body;


    let blockIsActive = localStorage.getItem("blockIsActive");
    if (blockIsActive === "true") {
        body.style.backgroundColor ='Blue';
    }


    btnChange.addEventListener('click', onChangeColorThemesClick);


    /**
     * @desc Функция event click для изменения темы на странице
     */



    function onChangeColorThemesClick() {


            localStorage.setItem("blockIsActive", "true");
           const bodyLocal = getComputedStyle(body).backgroundColor;

        if (!localStorage.getItem('bodyLocal')) {

            localStorage.setItem('bodyLocal', bodyLocal);

            let localBody = localStorage.getItem('bodyLocal');


            if (localBody === bodyLocal) {
                body.style.backgroundColor = '#FFD6DC';
            }

        } else {
            body.style.backgroundColor = localStorage.getItem('bodyLocal');
            localStorage.clear();
        }

    }

}