/**
 *
 * Функции нужны для того что бы не писать один и тот же код много раз на странице;
 * Передавать аргумен нужно для того что бы указать какие параметры должны выполняться в функции;
 */


let numFirst = prompt('Enter a first number', '0');
let numSecond = prompt('Enter a second number', '999');
let strOperator = prompt('Enter an arithmetic operators', '+, -, *, /');
let result = 0;


/**
 *
 * @desc Функция сумирование чисел
 * @param {Number}numberOne
 * @param {Number} numberTwo
 * @param {String} arithmeticOperator
 * @return {number}
 */

let addition = (numberOne, numberTwo, arithmeticOperator) => +numberOne + +numberTwo;

/**
 *
 * @desc Функция вычитание чисел
 * @param {Number}numberOne
 * @param {Number} numberTwo
 * @param {String} arithmeticOperator
 * @return {number}
 */

let subtraction = (numberOne, numberTwo, arithmeticOperator) => +numberOne - +numberTwo;
/**
 *
 * @desc Функция умножения чисел
 * @param {Number}numberOne
 * @param {Number} numberTwo
 * @param {String} arithmeticOperator
 * @return {number}
 */

let multiplication = (numberOne, numberTwo, arithmeticOperator) => +numberOne * +numberTwo;

/**
 *
 * @desc Функция деления чисел
 * @param {Number}numberOne
 * @param {Number} numberTwo
 * @param {String} arithmeticOperator
 * @return {number}
 */

let division = (numberOne, numberTwo, arithmeticOperator) => +numberOne / +numberTwo;




if (!+numFirst && !+numSecond || strOperator === '+' || strOperator === '-' || strOperator === '*' || strOperator === '/') {
    if (strOperator === '+') {
        result = addition(numFirst, numSecond, strOperator);
    } else if (strOperator === '-') {
        result = subtraction(numFirst, numSecond, strOperator);
    } else if (strOperator === '*') {
        result = multiplication(numFirst, numSecond, strOperator);
    } else if (strOperator === '/') {
        result = division(numFirst, numSecond, strOperator);
    }
} else {
    numFirst = prompt('Enter a correct  first number', '0');
    numSecond = prompt('Enter a correct second number', '999');
    strOperator = prompt('Enter a correct arithmetic operators', '+, -, *, /');

}


console.log(`${numFirst} ${strOperator} ${numSecond} = ${result}`);