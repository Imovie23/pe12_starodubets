document.addEventListener('DOMContentLoaded', onReady);

/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 *
 */
function onReady() {

    let numRadius;
    const body = document.body;
    const container = document.getElementById('container');
    let iDiv;

    /**
     * @desc Функция радиус круга и стили
     * @param {number} radius
     * @param elem
     */

    function circleRadius(radius, elem) {
        elem.style.cssText = `height : ${radius}px; width : ${radius}px; border-radius: 50%; background-color: ${buildColor()}; float:left; margin: 10px`;
    }

    /**
     * @desc Функция создание div на странице
     */

    function creatDiv() {
        for (let i = 0; i < 100; i++) {
            if (i < 100) {
                iDiv = document.createElement('div');
                container.appendChild(iDiv);
                circleRadius(numRadius, iDiv);

            } else {
                break;
            }

        }

    }

    const btnReady = document.getElementsByClassName('btnClick')[0];

    btnReady.addEventListener('click', onBtnClick);

    /**
     * @desc Функция onclick для кнопки, создание input и получение значение радиуса
     */

    function onBtnClick() {
        let inputValue = document.getElementById('inpVl');

        if (!inputValue) {
            inputValue = document.createElement('input');
            inputValue.id = 'inpVl';
            inputValue.type = 'text';
            body.appendChild(inputValue);
        } else {
            body.removeChild(btnReady);
            console.log(numRadius = inputValue.value);
            body.removeChild(inputValue);
            creatDiv();
        }

    }

    container.addEventListener('click', onContainerClick);

    /**
     * @desc Функция для удаление выбраного круга с страницы
     * @param event
     */

    function onContainerClick(event) {

        const element = event.target;
        console.log(element);

        if (event) {
            container.removeChild(element);
        }


    }

    /**
     * @desc Функция рандомного цвета
     *
     */

    function buildColor() {
        return `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`
    }

}